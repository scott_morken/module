<?php

namespace Module\Empty;

use Smorken\Module\AbstractModule;

class Module extends AbstractModule
{
    protected ?string $friendlyName = 'Empty Module';

    protected ?string $name = 'empty_module';
}
