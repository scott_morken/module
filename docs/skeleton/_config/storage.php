<?php

use Module\Empty\Contracts\Storage\Foo;

return [
    'core' => [
        \App\Contracts\Storage\Foo::class => Foo::class,
    ],
    'providers' => [
        \App\Contracts\Storage\Foo::class => Foo::class,
    ],
    'contract' => [
        Foo::class => \Module\Empty\Storage\Eloquent\Foo::class,
    ],
    'concrete' => [
        \Module\Empty\Storage\Eloquent\Foo::class => [
            'model' => [
                'impl' => \Module\Empty\Models\Eloquent\Foo::class,
            ],
        ],
    ],
];
