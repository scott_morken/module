<?php

namespace Module\Empty;

use App\Contracts\Modules\RelationProvider;
use App\Contracts\Modules\StorageProvider;
use App\Contracts\Modules\ViewProvider;
use App\Contracts\Storage\Enrollment;
use App\Contracts\Storage\Klass;
use App\Contracts\Storage\Person;
use App\Contracts\Storage\User;
use App\Contracts\Tools\PersonDetails;
use App\Import\CommandHandlers;
use Module\Empty\Contracts\Import\Parts\Classes;
use Module\Empty\Contracts\Import\Parts\Enrollments;
use Module\Empty\Contracts\Import\Parts\Instructors;
use Module\Empty\Contracts\Import\Parts\PeopleSports;
use Module\Empty\Contracts\Import\Parts\Sports;
use Module\Empty\Contracts\Import\Parts\Students;
use Module\Empty\Contracts\Storage\PersonSport;
use Module\Empty\Contracts\Storage\Sis\Term;
use Module\Empty\Contracts\Storage\Source\Athletes;
use Module\Empty\Contracts\Storage\Sport;
use Smorken\Import\Contracts\Enums\PartsFactoryParts;
use Smorken\Import\DataAndTargetModeller;
use Smorken\Import\Importers;
use Smorken\Import\PartsFactory;
use Smorken\Module\AbstractServiceProvider;

class ServiceProvider extends AbstractServiceProvider
{
    public function boot(): void
    {
        parent::boot();
        $this->commands([
            //FooCommand::class,
        ]);
    }

    public function register(): void
    {
        $this->bindAthletesStorage();
        $this->bindStorage($this->getModuleConfig()->get('storage', []));
        $this->bindStorageProvider();
        $this->bindCoreStorageProviders();
        $this->bindRelationProvider();
        $this->bindViewProvider();
        $this->bindPersonDetails();
        $this->bindAthletesImporter();
        $this->bindCommandHandler();
    }

    protected function addImportCommandHandler(): void
    {
        CommandHandlers::add(Module::getIdentifierName(), $this->app[CommandHandler::class]);
    }

    protected function bindAthletesImporter(): void
    {
        $this->app->bind(\Module\Empty\Contracts\Import\Athletes::class, function ($app) {
            $importerArray = [
                Sports::class => new \Module\Empty\Import\Parts\Sports(new PartsFactory([
                    PartsFactoryParts::DATA_TARGET_MODELLER => new DataAndTargetModeller,
                    PartsFactoryParts::DATA_MODEL => new \Module\Empty\Models\Import\Data\FromAthletes\Sport,
                    PartsFactoryParts::TARGET_PROVIDER => new \Module\Empty\Storage\Import\Target\Eloquent\Sport(new \Module\Empty\Models\Import\Target\Eloquent\Sport),
                    PartsFactoryParts::SOURCE_PROVIDER => $app[Athletes::class],
                ])),
                PeopleSports::class => new \Module\Empty\Import\Parts\PeopleSports(new PartsFactory([
                    PartsFactoryParts::DATA_TARGET_MODELLER => new DataAndTargetModeller,
                    PartsFactoryParts::DATA_MODEL => new \Module\Empty\Models\Import\Data\FromAthletes\PersonSport,
                    PartsFactoryParts::TARGET_PROVIDER => new \Module\Empty\Storage\Import\Target\Eloquent\PersonSport(new \Module\Empty\Models\Import\Target\Eloquent\PersonSport),
                    PartsFactoryParts::SOURCE_PROVIDER => $app[Athletes::class],
                ])),
                Students::class => new \Module\Empty\Import\Parts\Students(new PartsFactory([
                    PartsFactoryParts::DATA_TARGET_MODELLER => new DataAndTargetModeller,
                    PartsFactoryParts::DATA_MODEL => new \Module\Empty\Models\Import\Data\FromAthletes\Person,
                    PartsFactoryParts::TARGET_PROVIDER => new \Module\Empty\Storage\Import\Target\Eloquent\Person(new \Module\Empty\Models\Import\Target\Eloquent\Person),
                    PartsFactoryParts::SOURCE_PROVIDER => $app[Athletes::class],
                ])),
                Enrollments::class => new \Module\Empty\Import\Parts\Enrollments(new PartsFactory([
                    PartsFactoryParts::DATA_TARGET_MODELLER => new DataAndTargetModeller,
                    PartsFactoryParts::DATA_MODEL => new \Module\Empty\Models\Import\Data\FromAthletes\Enrollment,
                    PartsFactoryParts::TARGET_PROVIDER => new \Module\Empty\Storage\Import\Target\Eloquent\Enrollment(new \Module\Empty\Models\Import\Target\Eloquent\Enrollment),
                    PartsFactoryParts::SOURCE_PROVIDER => $app[Athletes::class],
                ])),
                Classes::class => new \Module\Empty\Import\Parts\Classes(new PartsFactory([
                    PartsFactoryParts::DATA_TARGET_MODELLER => new DataAndTargetModeller,
                    PartsFactoryParts::DATA_MODEL => new \Module\Empty\Models\Import\Data\FromAthletes\Klass,
                    PartsFactoryParts::TARGET_PROVIDER => new \Module\Empty\Storage\Import\Target\Eloquent\Klass(new \Module\Empty\Models\Import\Target\Eloquent\Klass),
                    PartsFactoryParts::SOURCE_PROVIDER => $app[Athletes::class],
                ])),
                Instructors::class => new \Module\Empty\Import\Parts\Instructors(new PartsFactory([
                    PartsFactoryParts::DATA_TARGET_MODELLER => new DataAndTargetModeller,
                    PartsFactoryParts::DATA_MODEL => new \Module\Empty\Models\Import\Data\FromAthletes\User,
                    PartsFactoryParts::TARGET_PROVIDER => new \Module\Empty\Storage\Import\Target\Eloquent\User(new \Module\Empty\Models\Import\Target\Eloquent\User),
                    PartsFactoryParts::SOURCE_PROVIDER => $app[Athletes::class],
                ])),
            ];
            $importers = new Importers($importerArray);

            return new \Module\Empty\Import\Athletes($importers, $app[Athletes::class]);
        });
    }

    protected function bindAthletesStorage(): void
    {
        $this->app->bind(Athletes::class, function ($app) {
            return new \Module\Empty\Storage\Import\Source\Eloquent\Athletes(
                new \Module\Empty\Models\Import\Source\Eloquent\Athletes
            );
        });
    }

    protected function bindCommandHandler(): void
    {
        $this->app->bind(CommandHandler::class, function ($app) {
            return new \Module\Empty\Import\CommandHandler($app[\Module\Empty\Contracts\Import\Athletes::class],
                $app[Term::class], 'PCC01');
        });
    }

    protected function bindCoreStorageProviders(): void
    {
        foreach ($this->getModuleConfig()->get('storage.core', []) as $coreInterface => $moduleInterface) {
            $this->app->bind($coreInterface, function ($app) use ($moduleInterface) {
                return $app[$moduleInterface];
            });
        }
    }

    protected function bindPersonDetails(): void
    {
        $this->app->bind(PersonDetails::class, function ($app) {
            return new \App\Tools\PersonDetails($app[Sport::class]);
        });
    }

    protected function bindRelationProvider(): void
    {
        $this->app->bind(RelationProvider::class, function ($app) {
            $relations = $this->getModuleConfig()->get('relations', []);

            return new \App\Modules\RelationProvider($relations);
        });
    }

    protected function bindStorage(array $storage): void
    {
        $binder = new \Smorken\Support\Binder($this->app);
        $binder->bindAll($storage);
    }

    protected function bindStorageProvider(): void
    {
        $this->app->bind(StorageProvider::class, function ($app) {
            $providers = [
                Enrollment::class => $app[\Module\Empty\Contracts\Storage\Foo::class],
                Klass::class => $app[\Module\Empty\Contracts\Storage\Klass::class],
                Person::class => $app[\Module\Empty\Contracts\Storage\Person::class],
                PersonSport::class => $app[PersonSport::class],
                Response::class => $app[Response::class],
                Sport::class => $app[Sport::class],
                User::class => $app[\Module\Empty\Contracts\Storage\User::class],
            ];

            return new \Module\Empty\Modules\StorageProvider($providers);
        });
    }

    protected function bindViewProvider(): void
    {
        $this->app->bind(ViewProvider::class, function ($app) {
            $views = $this->getModuleConfig()->get('views', []);

            return new \App\Modules\ViewProvider($views);
        });
    }
}
