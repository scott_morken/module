<?php

namespace Smorken\Module\Contracts;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Foundation\Application;

interface ModuleServiceProvider
{
    public function getApp(): Application;

    public function getModule(): Module;

    public function getModuleConfig(): Repository;
}
