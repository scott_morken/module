<?php

namespace Smorken\Module\Contracts;

use Illuminate\Contracts\Config\Repository;

interface Config
{
    public function setPath(string $path): void;

    public function get(): Repository;
}
