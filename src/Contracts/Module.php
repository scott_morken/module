<?php

namespace Smorken\Module\Contracts;

use Illuminate\Contracts\Foundation\Application;
use Psr\Log\LoggerInterface;

/**
 * @phpstan-require-extends \Smorken\Module\AbstractModule
 */
interface Module
{
    public function __construct(
        Application $application,
        LoggerInterface $logger,
        bool $debug = false
    );

    public function __invoke(): static;

    public function activate(...$params): void;

    public function getApp(): Application;

    public function getFriendlyName(): string;

    public function getName(): string;

    public function getNamespace(): string;

    public function isDefault(): bool;

    public function register(): void;

    public static function getIdentifierName(): string;
}
