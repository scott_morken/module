<?php

namespace Smorken\Module\Contracts;

interface Modules
{
    public function getDefaultModule(): Module;

    /**
     * @throws \Smorken\Module\ModuleException
     */
    public function getModule(string $class): Module;

    /**
     * @return array<string, \Smorken\Module\Contracts\Module>
     */
    public function getModules(): array;

    public function setModule(string $class, Module $module): void;

    public static function getCurrentModule(): ?Module;

    public static function setCurrentModule(Module $module): void;
}
