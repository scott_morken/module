<?php

namespace Smorken\Module\Contracts\Tools;

use Smorken\Module\Contracts\ModuleServiceProvider;

interface RegisterProviders
{
    public function __invoke(): void;

    public function getProviders(): array;

    public function getServiceProvider(): ModuleServiceProvider;
}
