<?php

namespace Smorken\Module\Contracts\Tools;

interface StorageProvider
{
    public function get(string $interface): mixed;
}
