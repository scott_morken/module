<?php

namespace Smorken\Module\Contracts\Tools;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Controller;
use Smorken\Module\Contracts\Module;

interface ModuleControllers
{
    public function convertToModuleController(Controller $controller): ?Controller;

    public function executeAction(Controller $appController, string $action, array $arguments): mixed;

    public function getApp(): Application;

    public function getModule(): Module;

    public function getModuleControllerClass(Controller $appController): string;
}
