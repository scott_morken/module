<?php

namespace Smorken\Module\Contracts\Tools\Models;

use Smorken\Module\Contracts\Tools\RelationProvider;

interface HasRelationProvider
{
    public function getRelationClassFromProvider(string $modelInterface, string $relationInterface): string;

    public function getRelationProvider(): RelationProvider;
}
