<?php

namespace Smorken\Module\Contracts\Tools\Models;

use Smorken\Module\Contracts\Module;
use Smorken\Module\Contracts\Modules;

interface HasModuleRelation
{
    public function getDefaultModule(): string;

    public function getModuleAttribute(): ?Module;

    public function getModuleKey(): string;

    public function getModules(): Modules;

    public static function bootHasModuleRelation(): void;
}
