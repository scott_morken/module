<?php

namespace Smorken\Module\Contracts\Tools;

interface ViewProvider
{
    public function getViewName(string $controllerClass, string $viewKey): string;

    public function setViewName(string $controllerClass, string $viewKey, string $viewName): void;

    /**
     * @param  array<string, string>  $views
     */
    public function setViewNames(string $controllerClass, array $views): void;
}
