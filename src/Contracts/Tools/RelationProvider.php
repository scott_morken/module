<?php

namespace Smorken\Module\Contracts\Tools;

interface RelationProvider
{
    public function addRelatedClasses(string $currentInterface, array $relatedClasses): void;

    public function addRelatedClass(string $currentInterface, string $relatedInterface, string $relatedConcrete): void;

    public function getRelatedClass(string $currentInterface, string $relatedInterface): string;
}
