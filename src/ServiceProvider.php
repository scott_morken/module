<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 2/12/15
 * Time: 8:40 AM
 */

namespace Smorken\Module;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Psr\Log\LoggerInterface;
use Smorken\Module\Contracts\Tools\ModuleControllers;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot(): void
    {
        $this->bootConfig();
        $this->app->make(\Smorken\Module\Contracts\Modules::class);
    }

    public function register(): void
    {
        $this->bindModules();
        $this->bindModuleControllers();
    }

    protected function bindModuleControllers(): void
    {
        $this->app->bind(ModuleControllers::class,
            fn ($app) => new \Smorken\Module\Tools\ModuleControllers($app, $app[ExceptionHandler::class]));
    }

    protected function bindModules(): void
    {
        $this->app->singleton(\Smorken\Module\Contracts\Modules::class, function ($app) {
            $moduleClasses = $app['config']->get('modules', []);

            return new Modules(
                $moduleClasses,
                $app,
                $app[LoggerInterface::class],
                $app['config']->get('app.debug', false)
            );
        });
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/modules.php';
        $app_path = config_path('modules.php');
        $this->mergeConfigFrom($config, 'modules');
        $this->publishes([$config => $app_path], 'config');
    }
}
