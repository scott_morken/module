<?php

namespace Smorken\Module\Tools;

class StorageProvider implements \Smorken\Module\Contracts\Tools\StorageProvider
{
    public function __construct(protected array $providers) {}

    public function get(string $interface): mixed
    {
        return $this->providers[$interface] ?? throw new \OutOfBoundsException("$interface is not a provider.");
    }
}
