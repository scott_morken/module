<?php

namespace Smorken\Module\Tools;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Controller;
use Smorken\Module\Contracts\Module;
use Smorken\Module\Modules;

class ModuleControllers implements \Smorken\Module\Contracts\Tools\ModuleControllers
{
    protected string $replaceNamespace = '/^[A-z0-9_\\\\]+(\\\\Http\\\\Controllers)/';

    public function __construct(
        protected Application $application,
        protected ExceptionHandler $exceptionHandler,
        ?string $replaceNamespace = null
    ) {
        if ($replaceNamespace) {
            $this->replaceNamespace = $replaceNamespace;
        }
    }

    public function convertToModuleController(Controller $controller): ?Controller
    {
        $moduleControllerClass = $this->getModuleControllerClass($controller);
        try {
            return $this->getApp()->make($moduleControllerClass);
        } catch (\Throwable) {
            return null;
        }
    }

    public function executeAction(Controller $appController, string $action, array $arguments): mixed
    {
        $moduleController = $this->convertToModuleController($appController);
        if ($moduleController) {
            try {
                return call_user_func_array([$moduleController, $action], $arguments);
            } catch (\Throwable $e) {
                $this->exceptionHandler->report($e);
            }
        }

        return false;
    }

    public function getApp(): Application
    {
        return $this->application;
    }

    public function getModule(): Module
    {
        return Modules::getCurrentModule();
    }

    public function getModuleControllerClass(Controller $appController): string
    {
        return $this->replaceNamespaceWithModuleNamespace($appController);
    }

    protected function replaceNamespaceWithModuleNamespace(Controller $controller): string
    {
        $moduleNamespace = $this->getModule()->getNamespace();

        return preg_replace($this->replaceNamespace, $moduleNamespace.'${1}', $controller::class, 1);
    }
}
