<?php

namespace Smorken\Module\Tools;

class RelationProvider implements \Smorken\Module\Contracts\Tools\RelationProvider
{
    protected array $relations = [];

    public function __construct(array $relations)
    {
        foreach ($relations as $modelInterface => $relatedClasses) {
            $this->addRelatedClasses($modelInterface, $relatedClasses);
        }
    }

    public function addRelatedClass(string $currentInterface, string $relatedInterface, string $relatedConcrete): void
    {
        $this->relations[$currentInterface][$relatedInterface] = $relatedConcrete;
    }

    public function addRelatedClasses(string $currentInterface, array $relatedClasses): void
    {
        foreach ($relatedClasses as $relatedInterface => $relatedConcrete) {
            $this->addRelatedClass($currentInterface, $relatedInterface, $relatedConcrete);
        }
    }

    public function getRelatedClass(string $currentInterface, string $relatedInterface): string
    {
        return $this->relations[$currentInterface][$relatedInterface];
    }
}
