<?php

namespace Smorken\Module\Tools;

use Illuminate\Contracts\Foundation\Application;
use Smorken\Module\Contracts\ModuleServiceProvider;
use Smorken\Module\Contracts\Tools\RelationProvider;
use Smorken\Support\Binder;

class RegisterProviders implements \Smorken\Module\Contracts\Tools\RegisterProviders
{
    public function __construct(protected ModuleServiceProvider $provider, protected array $providers) {}

    public function __invoke(): void
    {
        $this->bindRelationProvider();
        $this->bindViewProvider();
        $this->bindStorageViaBinder($this->getStorageForBinder());
        $this->bindCoreStorageProviders();
        $this->bindStorageProvider();
    }

    public function getProviders(): array
    {
        return $this->providers;
    }

    public function getServiceProvider(): ModuleServiceProvider
    {
        return $this->provider;
    }

    protected function bindCoreStorageProviders(): void
    {
        foreach ($this->getItemsForCoreStorage() as $coreInterface => $moduleInterface) {
            $this->getServiceProvider()->getApp()->bind($coreInterface, fn ($app) => $app[$moduleInterface]);
        }
    }

    protected function bindRelationProvider(): void
    {
        if ($this->getProviders()[RelationProvider::class] ?? null) {
            $this->getServiceProvider()->getApp()->bind(RelationProvider::class, function ($app) {
                $className = $this->getProviders()[RelationProvider::class];

                return new $className($this->getRelationsForRelationProvider($app));
            });
        }
    }

    protected function bindStorageProvider(): void
    {
        if ($this->getProviders()[\Smorken\Module\Contracts\Tools\StorageProvider::class] ?? null) {
            $this->getServiceProvider()
                ->getApp()
                ->bind(\Smorken\Module\Contracts\Tools\StorageProvider::class, function ($app) {
                    $className = $this->getProviders()[\Smorken\Module\Contracts\Tools\StorageProvider::class];

                    return new $className($this->getProvidersForStorageProvider($app));
                });
        }
    }

    protected function bindStorageViaBinder(array $storage): void
    {
        $binder = new Binder($this->getServiceProvider()->getApp());
        $binder->bindAll($storage);
    }

    protected function bindViewProvider(): void
    {
        if ($this->getProviders()[\Smorken\Module\Contracts\Tools\ViewProvider::class] ?? null) {
            $this->getServiceProvider()
                ->getApp()
                ->bind(\Smorken\Module\Contracts\Tools\ViewProvider::class, function ($app) {
                    $className = $this->getProviders()[\Smorken\Module\Contracts\Tools\ViewProvider::class];

                    return new $className($this->getViewsForViewProvider($app));
                });
        }
    }

    protected function getItemsForCoreStorage(): array
    {
        return $this->getServiceProvider()->getModuleConfig()->get('storage.core', []);
    }

    protected function getProvidersForStorageProvider(Application $app): array
    {
        $providers = [];
        $providerArray = $this->getServiceProvider()->getModuleConfig()->get('storage.providers', []);
        foreach ($providerArray as $fromInterface => $toInterface) {
            $providers[$fromInterface] = $app[$toInterface];
        }

        return $providers;
    }

    protected function getRelationsForRelationProvider(Application $app): array
    {
        return $this->getServiceProvider()->getModuleConfig()->get('relations', []);
    }

    protected function getStorageForBinder(): array
    {
        return $this->getServiceProvider()->getModuleConfig()->get('storage', []);
    }

    protected function getViewsForViewProvider(Application $app): array
    {
        return $this->getServiceProvider()->getModuleConfig()->get('views', []);
    }
}
