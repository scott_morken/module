<?php

namespace Smorken\Module\Tools\Models\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Smorken\Module\Contracts\Tools\Models\HasModuleRelation;

class ModuleIdScope implements Scope
{
    public function apply(Builder $builder, Model $model): void
    {
        $moduleId = $this->getCurrentModuleId($model);
        $key = $this->getModuleKey($model);
        if ($moduleId && $key) {
            $builder->where($key, '=', $moduleId);
        }
    }

    public function extend(Builder $builder): void
    {
        $this->addWithoutModuleId($builder);
    }

    protected function addWithoutModuleId(Builder $builder): void
    {
        // @phpstan-ignore argument.type
        $builder->macro('withoutModuleId', fn (Builder $builder) => $builder->withoutGlobalScope($this));
    }

    protected function getCurrentModuleId(Model|HasModuleRelation $model): ?string
    {
        if ($model instanceof HasModuleRelation) {
            $module = $model->getModules()->getCurrentModule();

            return $module?->getName();
        }

        return null;
    }

    protected function getModuleKey(Model|HasModuleRelation $model): ?string
    {
        if ($model instanceof HasModuleRelation) {
            return $model->getModuleKey();
        }

        return null;
    }
}
