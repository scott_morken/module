<?php

namespace Smorken\Module\Tools\Models\Traits;

use Illuminate\Support\Facades\App;
use Smorken\Module\Contracts\Tools\RelationProvider;

trait HasRelationProvider
{
    protected ?RelationProvider $relationProvider = null;

    public function getRelationClassFromProvider(string $modelInterface, string $relationInterface): string
    {
        return $this->getRelationProvider()->getRelatedClass($modelInterface, $relationInterface);
    }

    public function getRelationProvider(): RelationProvider
    {
        if (! $this->relationProvider) {
            $this->relationProvider = App::make(RelationProvider::class);
        }

        return $this->relationProvider;
    }
}
