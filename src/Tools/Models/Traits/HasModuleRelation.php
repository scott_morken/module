<?php

namespace Smorken\Module\Tools\Models\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\App;
use Smorken\Module\Contracts\Module;
use Smorken\Module\Contracts\Modules;
use Smorken\Module\Tools\Models\Scopes\ModuleIdScope;

trait HasModuleRelation
{
    protected ?Modules $modules = null;

    public static function bootHasModuleRelation(): void
    {
        static::addGlobalScope(new ModuleIdScope);
        static::saving(function ($model) {
            $key = $model->getModuleKey();
            if (! $model->getAttribute($key)) {
                $model->setAttribute($key, $model->getDefaultModule());
            }
        });
    }

    public function getDefaultModule(): string
    {
        return $this->getModules()->getCurrentModule()?->getName() ?? $this->getModules()
            ->getDefaultModule()
            ->getName();
    }

    public function getModuleAttribute(): ?Module
    {
        $module_id = $this->getAttribute($this->getModuleKey());
        try {
            return $this->getModules()->getModule($module_id);
        } catch (\Throwable) {
            return null;
        }
    }

    public function getModuleIdAttribute(): ?string
    {
        return $this->attributes[$this->getModuleKey()] ?? null;
    }

    public function getModuleKey(): string
    {
        if (property_exists($this, 'moduleKey')) {
            return $this->moduleKey;
        }

        return 'module_id';
    }

    public function getModules(): Modules
    {
        if (! $this->modules) {
            $this->modules = App::make(Modules::class);
        }

        return $this->modules;
    }

    public function scopeModuleIdIs(Builder $query, string $moduleId): Builder
    {
        return $query->where($this->getModuleKey(), '=', $moduleId);
    }

    public function setModuleIdAttribute(?string $value): void
    {
        $this->attributes[$this->getModuleKey()] = $value;
    }
}
