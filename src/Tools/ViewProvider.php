<?php

namespace Smorken\Module\Tools;

class ViewProvider implements \Smorken\Module\Contracts\Tools\ViewProvider
{
    protected array $views = [];

    public function __construct(array $views)
    {
        foreach ($views as $controllerClass => $viewItems) {
            $this->setViewNames($controllerClass, $viewItems);
        }
    }

    public function getViewName(string $controllerClass, string $viewKey): string
    {
        return $this->views[$controllerClass][$viewKey] ?? '';
    }

    public function setViewName(string $controllerClass, string $viewKey, string $viewName): void
    {
        $this->views[$controllerClass][$viewKey] = $viewName;
    }

    public function setViewNames(string $controllerClass, array $views): void
    {
        foreach ($views as $key => $name) {
            $this->setViewName($controllerClass, $key, $name);
        }
    }
}
