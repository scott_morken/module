<?php

namespace Smorken\Module;

use Illuminate\Contracts\Config\Repository;
use Illuminate\Contracts\Foundation\Application;
use Smorken\Module\Contracts\Module;
use Smorken\Module\Contracts\ModuleServiceProvider;
use Smorken\Module\Contracts\Tools\RelationProvider;
use Smorken\Module\Contracts\Tools\StorageProvider;
use Smorken\Module\Contracts\Tools\ViewProvider;
use Smorken\Module\Tools\RegisterProviders;

abstract class AbstractServiceProvider extends \Illuminate\Support\ServiceProvider implements ModuleServiceProvider
{
    protected ?string $basePath = null;

    protected array $providers = [
        RelationProvider::class => \Smorken\Module\Tools\RelationProvider::class,
        StorageProvider::class => \Smorken\Module\Tools\StorageProvider::class,
        ViewProvider::class => \Smorken\Module\Tools\ViewProvider::class,
    ];

    protected ?Repository $repository = null;

    public function __construct($app, protected Module $module)
    {
        parent::__construct($app);
    }

    public function boot(): void
    {
        $this->loadMigrations($this->getMigrationsPath());
        $this->loadRoutes($this->getRoutesPaths(['web.php', 'api.php']));
        $this->loadViews($this->getViewsPath(), $this->getViewsNamespace());
    }

    public function getApp(): Application
    {
        return $this->app;
    }

    public function getModule(): Module
    {
        return $this->module;
    }

    public function getModuleConfig(): Repository
    {
        if (is_null($this->repository)) {
            $this->repository = (new Config($this->getConfigPath()))->get();
        }

        return $this->repository;
    }

    protected function bindProviders(): void
    {
        (new RegisterProviders($this, $this->providers))();
    }

    protected function getBasePath(): string
    {
        if (is_null($this->basePath)) {
            $this->basePath = dirname((new \ReflectionClass($this))->getFileName());
        }

        return $this->basePath;
    }

    protected function getConfigPath(): string
    {
        return $this->getBasePath().'/_config';
    }

    protected function getMigrationsPath(): string|false
    {
        return realpath($this->getBasePath().'/Database/migrations');
    }

    protected function getRoutesPaths(array $routeFiles): array
    {
        $files = [];
        $path = realpath($this->getBasePath().'/_routes');
        if ($path) {
            foreach ($routeFiles as $file) {
                $routeFile = realpath($path.'/'.$file);
                if ($routeFile) {
                    $files[] = $routeFile;
                }
            }
        }

        return $files;
    }

    protected function getViewsNamespace(): string
    {
        return $this->getModule()->getName();
    }

    protected function getViewsPath(): string|false
    {
        return realpath($this->getBasePath().'/_resources/views');
    }

    protected function loadMigrations(string|false $path): void
    {
        if ($path) {
            $this->loadMigrationsFrom($path);
        }
    }

    protected function loadRoutes(array $routeFiles): void
    {
        foreach ($routeFiles as $routeFile) {
            $this->loadRoutesFrom($routeFile);
        }
    }

    protected function loadViews(string|false $path, string $namespace): void
    {
        if ($path) {
            $this->loadViewsFrom($path, $namespace);
        }
    }
}
