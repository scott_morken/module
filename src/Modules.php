<?php

namespace Smorken\Module;

use Illuminate\Contracts\Foundation\Application;
use Psr\Log\LoggerInterface;
use Smorken\Module\Contracts\Module;

class Modules implements \Smorken\Module\Contracts\Modules
{
    protected static ?Module $currentModule = null;

    /**
     * @var array<string, \Smorken\Module\Contracts\Module>
     */
    protected array $modules = [];

    public function __construct(
        array $modules,
        Application $application,
        LoggerInterface $logger,
        bool $debug
    ) {
        $this->buildModules($modules, $application, $logger, $debug);
    }

    public static function getCurrentModule(): ?Module
    {
        return self::$currentModule;
    }

    public static function setCurrentModule(Module $module): void
    {
        self::$currentModule = $module;
    }

    public function getDefaultModule(): Module
    {
        foreach ($this->getModules() as $module) {
            if ($module->isDefault()) {
                self::setCurrentModule($module);

                return $module;
            }
        }
        foreach ($this->getModules() as $module) {
            self::setCurrentModule($module);

            return $module;
        }
        throw new ModuleException('No modules have been added.');
    }

    public function getModule(string $class): Module
    {
        return $this->modules[$class] ?? $this->getModuleByName($class);
    }

    public function getModules(): array
    {
        return $this->modules;
    }

    public function setModule(string $class, Module $module): void
    {
        $this->modules[$class] = $module;
    }

    protected function buildModules(
        array $moduleClasses,
        Application $application,
        LoggerInterface $logger,
        bool $debug
    ): void {
        foreach ($moduleClasses as $moduleClass) {
            $module = (new $moduleClass($application, $logger, $debug))();
            $this->setModule($moduleClass, $module);
        }
    }

    protected function getModuleByName(string $name): Module
    {
        foreach ($this->modules as $module) {
            if ($module->getName() === $name) {
                return $module;
            }
        }
        throw new ModuleException("[$name] is not a module.");
    }
}
