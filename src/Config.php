<?php

namespace Smorken\Module;

use Illuminate\Contracts\Config\Repository;
use Symfony\Component\Finder\Finder;

class Config implements \Smorken\Module\Contracts\Config
{
    private string|false $path;

    public function __construct(string $path)
    {
        $this->setPath($path);
    }

    public function get(): Repository
    {
        return $this->getRepository();
    }

    public function setPath(string $path): void
    {
        $this->path = realpath($path);
    }

    protected function getConfigPath(): ?string
    {
        return $this->path;
    }

    /**
     * Get the configuration file nesting path.
     */
    protected function getNestedDirectory(\SplFileInfo $file, string $configPath): string
    {
        $directory = $file->getPath();

        if ($nested = trim(str_replace($configPath, '', $directory), DIRECTORY_SEPARATOR)) {
            $nested = str_replace(DIRECTORY_SEPARATOR, '.', $nested).'.';
        }

        return $nested;
    }

    protected function getRepository(): Repository
    {
        $path = $this->getConfigPath();
        $repository = new \Illuminate\Config\Repository([]);
        if ($path) {
            $files = $this->getConfigFiles($path);
            foreach ($files as $key => $filePath) {
                $repository->set($key, require $filePath);
            }
        }

        return $repository;
    }

    private function getConfigFiles(string $path): array
    {
        $files = [];
        foreach (Finder::create()->files()->name('*.php')->in($path) as $file) {
            $directory = $this->getNestedDirectory($file, $path);

            $files[$directory.basename($file->getRealPath(), '.php')] = $file->getRealPath();
        }

        ksort($files, SORT_NATURAL);

        return $files;
    }
}
