<?php

namespace Smorken\Module;

use Illuminate\Contracts\Foundation\Application;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Smorken\Module\Contracts\Module;
use Smorken\Module\Contracts\ModuleServiceProvider;

abstract class AbstractModule implements Module
{
    protected ?string $friendlyName = null;

    protected bool $isDefault = false;

    protected ?string $name = null;

    protected ?string $namespace = null;

    protected array $registerMethods = [];

    public function __construct(
        protected Application $application,
        protected LoggerInterface $logger,
        protected bool $debug = false
    ) {}

    public static function getIdentifierName(): string
    {
        $m = new static(\Illuminate\Foundation\Application::getInstance(), new NullLogger);

        return $m->getName();
    }

    public function __invoke(): static
    {
        $this->logToDebug('beginning registration');
        $this->register();

        return $this;
    }

    public function activate(...$params): void {}

    public function getApp(): Application
    {
        return $this->application;
    }

    public function getFriendlyName(): string
    {
        return $this->friendlyName ?? $this->getNameFromClass();
    }

    public function getName(): string
    {
        return $this->name ?? $this->getNameFromClass();
    }

    public function getNamespace(): string
    {
        if (is_null($this->namespace)) {
            $r = new \ReflectionClass($this);
            $this->namespace = $r->getNamespaceName();
        }

        return $this->namespace;
    }

    public function isDefault(): bool
    {
        return $this->isDefault;
    }

    public function register(): void
    {
        $this->registerServiceProvider($this->getServiceProviderClass());
        $this->registerViaMethods($this->registerMethods);
        $this->logToDebug('registration complete');
    }

    protected function getNameFromClass(): string
    {
        return str_replace('\\', '', $this::class);
    }

    protected function getRegisterableServiceProvider(string $serviceProviderClass): ModuleServiceProvider|string
    {
        if (in_array(ModuleServiceProvider::class, class_implements($serviceProviderClass))) {
            return new $serviceProviderClass($this->getApp(), $this);
        }

        return $serviceProviderClass;
    }

    protected function getServiceProviderClass(): string
    {
        $reflection = new \ReflectionClass($this);

        return $reflection->getNamespaceName().'\\ServiceProvider';
    }

    protected function logToDebug(string $message): void
    {
        if ($this->debug) {
            $this->logger->debug(sprintf('[MODULE][%s]: %s', $this->getName(), $message));
        }
    }

    protected function registerServiceProvider(string $serviceProviderClass): void
    {
        if (class_exists($serviceProviderClass)) {
            $this->getApp()->register($this->getRegisterableServiceProvider($serviceProviderClass));
            $this->logToDebug('registered: '.$serviceProviderClass);
        }
    }

    protected function registerViaMethods(array $methods): void
    {
        foreach ($methods as $method) {
            if (method_exists($this, $method)) {
                $this->$method();
                $this->logToDebug('registered: '.$method);
            }
        }
    }
}
