<?php

namespace Tests\Smorken\Module\Stubs\Models;

use Illuminate\Database\Eloquent\Model;

class RelationConcrete extends Model implements RelationContract {}
