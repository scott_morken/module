<?php

namespace Tests\Smorken\Module\Stubs\Models;

use Illuminate\Database\Eloquent\Model;
use Smorken\Module\Contracts\Tools\Models\HasModuleRelation;

class ModelWithModuleIdScope extends Model implements HasModuleRelation
{
    use \Smorken\Module\Tools\Models\Traits\HasModuleRelation;

    protected $fillable = ['foo', 'module_id'];

    protected $table = 'table';
}
