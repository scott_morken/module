<?php

namespace Tests\Smorken\Module\Stubs\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Smorken\Module\Contracts\Tools\Models\HasRelationProvider;

class ModelWithRelationProvider extends Model implements HasRelationProvider, ModelWithRelationProviderContract
{
    use \Smorken\Module\Tools\Models\Traits\HasRelationProvider;

    public function myRelation(): HasMany
    {
        return $this->hasMany(
            $this->getRelationClassFromProvider(
                ModelWithRelationProviderContract::class,
                RelationContract::class
            )
        );
    }
}
