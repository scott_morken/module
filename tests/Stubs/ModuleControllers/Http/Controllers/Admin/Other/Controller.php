<?php

namespace Tests\Smorken\Module\Stubs\ModuleControllers\Http\Controllers\Admin\Other;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Smorken\Module\Contracts\Tools\ModuleControllers;

class Controller extends \Illuminate\Routing\Controller
{
    public function __construct(protected ModuleControllers $moduleControllers) {}

    public function index(Request $request): Response
    {
        return $this->moduleControllers->executeAction($this, 'index', func_get_args());
    }
}
