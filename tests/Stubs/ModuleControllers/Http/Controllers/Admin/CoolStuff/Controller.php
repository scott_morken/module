<?php

namespace Tests\Smorken\Module\Stubs\ModuleControllers\Http\Controllers\Admin\CoolStuff;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Smorken\Module\Contracts\Tools\ModuleControllers;

class Controller extends \Illuminate\Routing\Controller
{
    public function __construct(protected ModuleControllers $moduleControllers) {}

    public function index(Request $request, int $id): Response
    {
        $response = $this->moduleControllers->executeAction($this, 'index', func_get_args());
        if ($response !== false) {
            return $response;
        }

        return \Illuminate\Support\Facades\Response::make('Not found.');
    }
}
