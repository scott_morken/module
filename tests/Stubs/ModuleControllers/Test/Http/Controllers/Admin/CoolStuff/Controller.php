<?php

namespace Tests\Smorken\Module\Stubs\ModuleControllers\Test\Http\Controllers\Admin\CoolStuff;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class Controller extends \Illuminate\Routing\Controller
{
    public function index(Request $request, int $id): Response
    {
        return \Illuminate\Support\Facades\Response::make("id=$id");
    }
}
