<?php

namespace Tests\Smorken\Module\Stubs\Test;

use Smorken\Module\AbstractModule;

class Module extends AbstractModule
{
    public bool $something = false;

    protected ?string $friendlyName = 'Test Module';

    protected bool $isDefault = true;

    protected ?string $name = 'test_module';

    protected array $registerMethods = [
        'registerSomething',
    ];

    protected function registerSomething(): void
    {
        $this->something = true;
    }
}
