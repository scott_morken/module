<?php

namespace Tests\Smorken\Module\Stubs;

use Illuminate\Contracts\Config\Repository;
use Smorken\Module\AbstractServiceProvider;

class ModuleServiceProvider extends AbstractServiceProvider
{
    public function getConfig(): Repository
    {
        return $this->getModuleConfig();
    }
}
