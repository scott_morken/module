<?php

namespace Tests\Smorken\Module\Unit;

use Tests\Smorken\Module\Stubs\ModuleServiceProvider;
use Tests\Smorken\Module\Stubs\Test\Module;

class ModuleServiceProviderTest extends BaseTestClass
{
    public function testConfigFromCorrectDirectory(): void
    {
        $sut = $this->getSut();
        $config = $sut->getConfig();
        $this->assertEquals(1, $config->get('conf1.c1'));
        $this->assertEquals('2', $config->get('conf2.c2'));
    }

    protected function getSut(): ModuleServiceProvider
    {
        return new ModuleServiceProvider($this->getApp(), new Module($this->getApp(), $this->getLogger(), true));
    }
}
