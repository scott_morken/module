<?php

namespace Tests\Smorken\Module\Unit\Traits;

use Tests\Smorken\Module\Stubs\Test\ServiceProvider;

trait HasModuleWhen
{
    protected function whenNoNameModuleRegistered(): void
    {
        $this->getLogger()->shouldReceive('debug')
            ->once()
            ->with('[MODULE][TestsSmorkenModuleStubsNoNameModule]: beginning registration');
        $this->getApp()->shouldReceive('register')
            ->never();
        $this->getLogger()->shouldReceive('debug')
            ->once()
            ->with('[MODULE][TestsSmorkenModuleStubsNoNameModule]: registration complete');
    }

    protected function whenNoNameModuleTwoRegistered(): void
    {
        $this->getLogger()->shouldReceive('debug')
            ->once()
            ->with('[MODULE][TestsSmorkenModuleStubsNoNameModuleTwo]: beginning registration');
        $this->getApp()->shouldReceive('register')
            ->never();
        $this->getLogger()->shouldReceive('debug')
            ->once()
            ->with('[MODULE][TestsSmorkenModuleStubsNoNameModuleTwo]: registration complete');
    }

    protected function whenTestModuleRegistered(): void
    {
        $this->getLogger()->shouldReceive('debug')
            ->once()
            ->with('[MODULE][test_module]: beginning registration');
        $this->getApp()->shouldReceive('register')
            ->once()
            ->with(ServiceProvider::class);
        $this->getLogger()->shouldReceive('debug')
            ->once()
            ->with('[MODULE][test_module]: registered: Tests\Smorken\Module\Stubs\Test\ServiceProvider');
        $this->getLogger()->shouldReceive('debug')
            ->once()
            ->with('[MODULE][test_module]: registered: registerSomething');
        $this->getLogger()->shouldReceive('debug')
            ->once()
            ->with('[MODULE][test_module]: registration complete');
    }
}
