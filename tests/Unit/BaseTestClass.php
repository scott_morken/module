<?php

namespace Tests\Smorken\Module\Unit;

use Illuminate\Contracts\Foundation\Application;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

abstract class BaseTestClass extends TestCase
{
    protected ?Application $application = null;

    protected ?LoggerInterface $logger = null;

    protected function getApp(): Application
    {
        if (is_null($this->application)) {
            $this->application = m::mock(Application::class);
        }

        return $this->application;
    }

    protected function getLogger(): LoggerInterface
    {
        if (is_null($this->logger)) {
            $this->logger = m::mock(LoggerInterface::class);
        }

        return $this->logger;
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
