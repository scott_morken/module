<?php

namespace Tests\Smorken\Module\Unit;

use Smorken\Module\Contracts\Modules;
use Smorken\Module\ModuleException;
use Tests\Smorken\Module\Stubs\NoName\Module;
use Tests\Smorken\Module\Stubs\NoName\ModuleTwo;
use Tests\Smorken\Module\Unit\Traits\HasModuleWhen;

class ModulesTest extends BaseTestClass
{
    use HasModuleWhen;

    public function testGetDefaultModule(): void
    {
        $this->whenNoNameModuleRegistered();
        $this->whenTestModuleRegistered();
        $sut = $this->getSut($this->getModules());
        $modules = $sut->getModules();
        $this->assertCount(2, $modules);
        $this->assertInstanceOf(\Tests\Smorken\Module\Stubs\Test\Module::class, $sut->getDefaultModule());
        $this->assertInstanceOf(\Tests\Smorken\Module\Stubs\Test\Module::class, $sut::getCurrentModule());
    }

    public function testGetDefaultModuleWithNoDefaultIsFirst(): void
    {
        $this->whenNoNameModuleRegistered();
        $this->whenNoNameModuleTwoRegistered();
        $sut = $this->getSut([
            Module::class,
            ModuleTwo::class,
        ]);
        $modules = $sut->getModules();
        $this->assertCount(2, $modules);
        $this->assertInstanceOf(Module::class, $sut->getDefaultModule());
        $this->assertInstanceOf(Module::class, $sut::getCurrentModule());
    }

    public function testGetDefaultModuleWithNoModulesThrowsException(): void
    {
        $sut = $this->getSut([]);
        $modules = $sut->getModules();
        $this->assertCount(0, $modules);
        $this->expectException(ModuleException::class);
        $this->expectExceptionMessage('No modules have been added.');
        $sut->getDefaultModule();
    }

    public function testGetModules(): void
    {
        $this->whenNoNameModuleRegistered();
        $this->whenTestModuleRegistered();
        $sut = $this->getSut($this->getModules());
        $modules = $sut->getModules();
        $this->assertCount(2, $modules);
        $this->assertArrayHasKey(Module::class, $modules);
        $this->assertArrayHasKey(\Tests\Smorken\Module\Stubs\Test\Module::class, $modules);
    }

    public function testModulesAreRegistered(): void
    {
        $this->whenNoNameModuleRegistered();
        $this->whenTestModuleRegistered();
        $sut = $this->getSut($this->getModules());
        $this->assertTrue($sut->getModule('test_module')->something);
        $this->assertInstanceOf(Module::class, $sut->getModule(Module::class));
    }

    public function testModulesGetModuleNoModuleExistsIsException(): void
    {
        $sut = $this->getSut([]);
        $this->expectException(ModuleException::class);
        $this->expectExceptionMessage('[test_module] is not a module.');
        $sut->getModule('test_module');
    }

    protected function getModules(): array
    {
        return [
            Module::class,
            \Tests\Smorken\Module\Stubs\Test\Module::class,
        ];
    }

    protected function getSut(array $modules): Modules
    {
        return new \Smorken\Module\Modules($modules, $this->getApp(), $this->getLogger(), true);
    }
}
