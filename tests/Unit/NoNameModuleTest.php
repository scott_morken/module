<?php

namespace Tests\Smorken\Module\Unit;

use Tests\Smorken\Module\Stubs\NoName\Module;
use Tests\Smorken\Module\Unit\Traits\HasModuleWhen;

class NoNameModuleTest extends BaseTestClass
{
    use HasModuleWhen;

    public function testFriendlyName(): void
    {
        $sut = $this->getSut();
        $this->assertEquals('TestsSmorkenModuleStubsNoNameModule', $sut->getFriendlyName());
    }

    public function testInvokeRegistersModule(): void
    {
        $this->whenNoNameModuleRegistered();
        $sut = $this->getSut()();
        $this->assertInstanceOf(Module::class, $sut);
    }

    public function testName(): void
    {
        $sut = $this->getSut();
        $this->assertEquals('TestsSmorkenModuleStubsNoNameModule', $sut->getName());
    }

    public function testNamespace(): void
    {
        $sut = $this->getSut();
        $this->assertEquals('Tests\Smorken\Module\Stubs\NoName', $sut->getNamespace());
    }

    public function testStaticName(): void
    {
        $this->assertEquals('TestsSmorkenModuleStubsNoNameModule', Module::getIdentifierName());
    }

    protected function getSut(): \Smorken\Module\Contracts\Module
    {
        return new Module($this->getApp(), $this->getLogger(), true);
    }
}
