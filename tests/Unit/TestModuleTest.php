<?php

namespace Tests\Smorken\Module\Unit;

use Smorken\Module\Contracts\Module;
use Tests\Smorken\Module\Unit\Traits\HasModuleWhen;

class TestModuleTest extends BaseTestClass
{
    use HasModuleWhen;

    public function testFriendlyName(): void
    {
        $sut = $this->getSut();
        $this->assertEquals('Test Module', $sut->getFriendlyName());
    }

    public function testInvokeRegistersModule(): void
    {
        $this->whenTestModuleRegistered();
        $sut = $this->getSut()();
        $this->assertInstanceOf(\Tests\Smorken\Module\Stubs\Test\Module::class, $sut);
        $this->assertTrue($sut->something);
    }

    public function testName(): void
    {
        $sut = $this->getSut();
        $this->assertEquals('test_module', $sut->getName());
    }

    public function testNamespace(): void
    {
        $sut = $this->getSut();
        $this->assertEquals('Tests\Smorken\Module\Stubs\Test', $sut->getNamespace());
    }

    public function testStaticName(): void
    {
        $this->assertEquals('test_module', \Tests\Smorken\Module\Stubs\Test\Module::getIdentifierName());
    }

    protected function getSut(): Module
    {
        return new \Tests\Smorken\Module\Stubs\Test\Module($this->getApp(), $this->getLogger(), true);
    }
}
