<?php

namespace Tests\Smorken\Module\Unit\Tools\Models;

use Illuminate\Container\Container;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Events\Dispatcher;
use Illuminate\Support\Facades\App;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Module\Contracts\Module;
use Smorken\Module\Contracts\Modules;
use Smorken\Module\Tools\Models\Scopes\ModuleIdScope;
use Tests\Smorken\Module\Stubs\Models\ModelWithModuleIdScope;

class HasModuleRelationTest extends TestCase
{
    public function testDefaultModuleIdIsAppliedToCreatedModelWhenNotSet(): void
    {
        $modules = m::mock(Modules::class);
        $module = m::mock(Module::class);
        App::shouldReceive('make')
            ->once()
            ->with(Modules::class)
            ->andReturn($modules);
        $modules->shouldReceive('getCurrentModule')
            ->andReturn($module);
        $module->shouldReceive('getName')
            ->andReturn('my-module');
        $model = new ModelWithModuleIdScope(['foo' => 'bar']);
        try {
            $model->save();
        } catch (QueryException $e) {
            $this->assertEquals(
                'insert into "table" ("foo", "module_id", "updated_at", "created_at") values (?, ?, ?, ?)',
                $e->getSql()
            );
            $this->assertTrue(in_array('bar', $e->getBindings()));
            $this->assertTrue(in_array('my-module', $e->getBindings()));
        }
    }

    public function testGlobalScopeCanBeRemoved(): void
    {
        $model = new ModelWithModuleIdScope;
        $query = $model->newQuery()->withoutGlobalScope(ModuleIdScope::class);
        $this->assertSame('select * from "table"', $query->toSql());
        $this->assertEquals([], $query->getBindings());
    }

    public function testGlobalScopeIsApplied(): void
    {
        $modules = m::mock(Modules::class);
        $module = m::mock(Module::class);
        App::shouldReceive('make')
            ->once()
            ->with(Modules::class)
            ->andReturn($modules);
        $modules->shouldReceive('getCurrentModule')
            ->andReturn($module);
        $module->shouldReceive('getName')
            ->andReturn('my-module');
        $model = new ModelWithModuleIdScope;
        $query = $model->newQuery();
        $this->assertSame('select * from "table" where "module_id" = ?', $query->toSql());
        $this->assertEquals(['my-module'], $query->getBindings());
    }

    protected function setUp(): void
    {
        parent::setUp();

        tap(new DB)->addConnection([
            'driver' => 'sqlite',
            'database' => ':memory:',
        ])->bootEloquent();
        Model::setEventDispatcher(new Dispatcher(Container::getInstance()));
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        Model::unsetConnectionResolver();
    }
}
