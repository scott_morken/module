<?php

namespace Tests\Smorken\Module\Unit\Tools\Models;

use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Smorken\Module\Tools\RelationProvider;
use Tests\Smorken\Module\Stubs\Models\ModelWithRelationProvider;
use Tests\Smorken\Module\Stubs\Models\ModelWithRelationProviderContract;
use Tests\Smorken\Module\Stubs\Models\RelationConcrete;
use Tests\Smorken\Module\Stubs\Models\RelationContract;

class HasRelationProviderTest extends TestCase
{
    public function testRelationSelectsCorrectModel(): void
    {
        $relationProvider = new RelationProvider([
            ModelWithRelationProviderContract::class => [
                RelationContract::class => RelationConcrete::class,
            ],
        ]);
        App::shouldReceive('make')
            ->once()
            ->with(\Smorken\Module\Contracts\Tools\RelationProvider::class)
            ->andReturn($relationProvider);
        $model = (new ModelWithRelationProvider)->forceFill(['id' => 1]);
        $query = $model->myRelation();
        $this->assertEquals('select * from "relation_concretes" where "relation_concretes"."model_with_relation_provider_id" = ? and "relation_concretes"."model_with_relation_provider_id" is not null',
            $query->toSql());
        $this->assertEquals([1], $query->getBindings());
    }

    protected function setUp(): void
    {
        parent::setUp();

        tap(new DB)->addConnection([
            'driver' => 'sqlite',
            'database' => ':memory:',
        ])->bootEloquent();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
        Model::unsetConnectionResolver();
    }
}
