<?php

namespace Tests\Smorken\Module\Unit\Tools;

use PHPUnit\Framework\TestCase;
use Smorken\Module\Tools\StorageProvider;

class StorageProviderTest extends TestCase
{
    public function testGetCanRetrieveProvider(): void
    {
        $o = new \stdClass;
        $sut = new StorageProvider(['foo' => $o]);
        $this->assertSame($o, $sut->get('foo'));
    }

    public function testGetIsErrorWhenMissingKey(): void
    {
        $o = new \stdClass;
        $sut = new StorageProvider(['foo' => $o]);
        $this->expectException(\OutOfBoundsException::class);
        $this->expectExceptionMessage('bar is not a provider.');
        $sut->get('bar');
    }
}
