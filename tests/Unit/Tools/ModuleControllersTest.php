<?php

namespace Tests\Smorken\Module\Unit\Tools;

use Illuminate\Contracts\Debug\ExceptionHandler;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Mockery as m;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Smorken\Module\Contracts\Tools\ModuleControllers;
use Smorken\Module\Modules;
use Tests\Smorken\Module\Stubs\ModuleControllers\Http\Controllers\Admin\CoolStuff\Controller;

class ModuleControllersTest extends TestCase
{
    protected ?Application $application = null;

    protected ?ExceptionHandler $exceptionHandler = null;

    protected ?LoggerInterface $logger = null;

    public function testExecuteAction(): void
    {
        $modules = $this->getModules();
        Modules::setCurrentModule($modules->getDefaultModule());
        $sut = $this->getSut();
        $controller = new Controller($sut);
        $this->getApp()->shouldReceive('make')
            ->once()
            ->with(\Tests\Smorken\Module\Stubs\ModuleControllers\Test\Http\Controllers\Admin\CoolStuff\Controller::class)
            ->andReturn(new \Tests\Smorken\Module\Stubs\ModuleControllers\Test\Http\Controllers\Admin\CoolStuff\Controller);
        $responseMock = m::mock(Response::class);
        \Illuminate\Support\Facades\Response::shouldReceive('make')
            ->once()
            ->with('id=1')
            ->andReturn($responseMock);
        $response = $sut->executeAction($controller, 'index', ['request' => new Request, 'id' => 1]);
        $this->assertEquals($responseMock, $response);
    }

    public function testExecuteActionNoActionFoundIsFalse(): void
    {
        $modules = $this->getModules();
        Modules::setCurrentModule($modules->getDefaultModule());
        $sut = $this->getSut();
        $controller = new Controller($sut);
        $this->getApp()->shouldReceive('make')
            ->once()
            ->with(\Tests\Smorken\Module\Stubs\ModuleControllers\Test\Http\Controllers\Admin\CoolStuff\Controller::class)
            ->andReturn(new \Tests\Smorken\Module\Stubs\ModuleControllers\Test\Http\Controllers\Admin\CoolStuff\Controller);
        $this->getExceptionHandler()->shouldReceive('report')
            ->once()
            ->with(m::type(\BadMethodCallException::class));
        $response = $sut->executeAction($controller, 'view', ['id' => 1]);
        $this->assertFalse($response);
    }

    public function testExecuteActionNoModuleControllerIsFalse(): void
    {
        $modules = $this->getModules();
        Modules::setCurrentModule($modules->getDefaultModule());
        $sut = $this->getSut();
        $controller = new \Tests\Smorken\Module\Stubs\ModuleControllers\Http\Controllers\Admin\Other\Controller($sut);
        $response = $sut->executeAction($controller, 'index', ['request' => new Request]);
        $this->assertFalse($response);
    }

    public function testReplaceNamespace(): void
    {
        $modules = $this->getModules();
        Modules::setCurrentModule($modules->getDefaultModule());
        $sut = $this->getSut();
        $controller = new Controller($sut);
        $this->assertEquals(\Tests\Smorken\Module\Stubs\ModuleControllers\Test\Http\Controllers\Admin\CoolStuff\Controller::class,
            $sut->getModuleControllerClass($controller));
    }

    protected function getApp(): Application
    {
        if (is_null($this->application)) {
            $this->application = m::mock(Application::class);
        }

        return $this->application;
    }

    protected function getExceptionHandler(): ExceptionHandler
    {
        if (is_null($this->exceptionHandler)) {
            $this->exceptionHandler = m::mock(ExceptionHandler::class);
        }

        return $this->exceptionHandler;
    }

    protected function getLogger(): LoggerInterface
    {
        if (is_null($this->logger)) {
            $this->logger = m::mock(LoggerInterface::class);
        }

        return $this->logger;
    }

    protected function getModules(array $modules = []): \Smorken\Module\Contracts\Modules
    {
        if (empty($modules)) {
            $modules = [
                \Tests\Smorken\Module\Stubs\ModuleControllers\Test\Module::class,

            ];
        }

        return new Modules(
            $modules,
            $this->getApp(),
            $this->getLogger(),
            false
        );
    }

    protected function getSut(): ModuleControllers
    {
        return new \Smorken\Module\Tools\ModuleControllers($this->getApp(), $this->getExceptionHandler());
    }

    protected function tearDown(): void
    {
        parent::tearDown();
        m::close();
    }
}
