<?php

namespace Tests\Smorken\Module\Unit\Tools;

use PHPUnit\Framework\TestCase;
use Smorken\Module\Tools\ViewProvider;
use Tests\Smorken\Module\Stubs\ModuleControllers\Http\Controllers\Admin\CoolStuff\Controller;

class ViewProviderTest extends TestCase
{
    public function testCanFindViewName(): void
    {
        $sut = new ViewProvider([
            Controller::class => [
                'index' => 'module::index',
            ],
        ]);
        $this->assertEquals('module::index', $sut->getViewName(Controller::class, 'index'));
    }

    public function testMissingViewIsEmptyString(): void
    {
        $sut = new ViewProvider([
            Controller::class => [
                'index' => 'module::index',
            ],
        ]);
        $this->assertEquals('', $sut->getViewName(Controller::class, 'view'));
    }
}
