<?php

namespace Tests\Smorken\Module\Unit\Tools;

use PHPUnit\Framework\TestCase;
use Smorken\Module\Tools\RelationProvider;
use Tests\Smorken\Module\Stubs\Models\ModelWithRelationProviderContract;
use Tests\Smorken\Module\Stubs\Models\RelationConcrete;
use Tests\Smorken\Module\Stubs\Models\RelationContract;

class RelationProviderTest extends TestCase
{
    public function testGetRelatedClassReturnsClassNameString(): void
    {
        $sut = new RelationProvider([
            ModelWithRelationProviderContract::class => [
                RelationContract::class => RelationConcrete::class,
            ],
        ]);
        $this->assertEquals(RelationConcrete::class,
            $sut->getRelatedClass(ModelWithRelationProviderContract::class, RelationContract::class));
    }
}
