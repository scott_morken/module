## Module loader for Laravel 9

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Installation

It should register itself automatically. If not, add to your `config/app.php`:

    ...
    'providers' => [
            Smorken\Module\ServiceProvider::class,
    ...

### Use

Add another namespace to your app (or create another directory under your app's namespace) for
modules.  

`composer.json`
```json
"autoload": {
    "psr-4": {
      "App\\": "app/",
      "Module\\": "modules/module/",
```

Update `config/modules.php`
```php
<?php
return [
    \Module\PC\Foo\Module::class,
    \Module\PC\Bar\Module::class,
];
```

Directory structure:

* modules/module/PC/Foo
  * _config
    * config.php
    * relations.php (`RelationProvider`)
    * storage.php (core providers, `Binder`, `StorageProvider`)
    * views.php (`ViewProvider`)
  * _resources
    * views
  * _routes
    * api.php (if needed)
    * web.php (if needed)
  * Commands
  * Contracts
  * Database
    * migrations
    * Seeders
  * Http
  * Models
  * Storage
  * Module.php - registers module (via service provider; `registerMethods` property)
  * ServiceProvider.php - initialized by Module.php
